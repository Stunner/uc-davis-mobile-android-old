/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android;

public interface AppConstants {

	// Logging tag
	public static final String UCDM_LOG_TAG = "** UCDM **";
	
	// Actions (events)
	public static final int ACTION_SHOW_OVERFLOW_ACTIONS = 0;
	public static final int ACTION_SHOW_DASHBOARD = 1;
	public static final int ACTION_SHOW_EMAIL_SERVICES = 2;
	public static final int ACTION_SHOW_EMAIL_SERVCIE_DETAIL = 3;
	public static final int ACTION_SHOW_PARKING_LOTS = 4;
	public static final int ACTION_SHOW_PARKING_LOT_DETAIL = 5;
	
	// Help context
	public static final int HELP_CONTEXT_DASHBOARD = 0;
	public static final int HELP_CONTEXT_EMAIL_SERVICES = 0;
	
	// Overflow actions
	// NOTE: The help action will be in the fist position in all the lists
	public static final int LIST_ACTION_HELP = 0;
	
	// Dashboard overflow actions : starting at 1
	public static final int LIST_ACTION_INFO = 1;
	public static final int LIST_ACTION_PARKING = 2;

	// Parking Lots overflow actions: starting at 1
	// ...
	
	/*
	 * ERROR CODES:
	 */
	
	// GetContentTask
	public static final int NO_ERROR = 0;
	public static final int REST_ERROR = 1;
	
	/*
	 * CONTENT CACHE KEYS (CCK):
	 */
	public static final Integer PARKING_LOT_CCK = Integer.valueOf(0);
}
