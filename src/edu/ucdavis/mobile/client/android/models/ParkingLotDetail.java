/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.models;

public class ParkingLotDetail extends ParkingLot {

	public ParkingLotDetail(String name) {
		super(name);
	}

}
