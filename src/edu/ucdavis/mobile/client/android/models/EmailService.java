/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import edu.ucdavis.mobile.client.android.AppConstants;

public class EmailService {
	
	String mName;
	String mUrl;
	
	public EmailService(JSONObject data) {

		try {

			mName = data.getString("name");
			mUrl = data.getString("url");
		}
		catch (JSONException e) {
			
			Log.e(AppConstants.UCDM_LOG_TAG, "JSONException: EmailService");
		}
	}

	public String getName() {
		
		return mName;
	}

	public String getUrl() {

		return mUrl;
	}
	
	@Override
	public String toString() {
		
		return mName;
	}
}
