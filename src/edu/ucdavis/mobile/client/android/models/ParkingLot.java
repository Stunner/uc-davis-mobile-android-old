/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.models;

public class ParkingLot {

	String mName;
	
	public ParkingLot(String name) {
		
		mName = name;
	}
	
	public String getName() {
		
		return mName;
	}
	
	@Override
	public String toString() {
		
		return mName;
	}
}
