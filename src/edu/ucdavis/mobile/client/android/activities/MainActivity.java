/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import edu.ucdavis.mobile.client.android.ActionBusListener;
import edu.ucdavis.mobile.client.android.AppConstants;
import edu.ucdavis.mobile.client.android.MainApplication;
import edu.ucdavis.mobile.client.android.R;
import edu.ucdavis.mobile.client.android.fragments.DashboardActionbarFragment;
import edu.ucdavis.mobile.client.android.fragments.DashboardFragment;

public class MainActivity extends FragmentActivity implements ActionBusListener, AppConstants {

	private MainApplication mApplication;
	
	private DashboardActionbarFragment mDashboardActionbarFragment;
	private DashboardFragment mDashboardFragment;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        
        mApplication = (MainApplication) getApplication();
        mApplication.initActionBusListener();
        
        // Register action events
		mApplication.registerAction(this,
				ACTION_SHOW_DASHBOARD,
				ACTION_SHOW_EMAIL_SERVICES,
				ACTION_SHOW_PARKING_LOTS);
        
        showDashboard();
    }
	
	protected void showDashboard() {
		
		FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        
        if(null == mDashboardActionbarFragment) {
        	
        	mDashboardActionbarFragment = DashboardActionbarFragment.newInstance();
        	fragmentTransaction.replace(R.id.actionbar_container, mDashboardActionbarFragment);
        }
        
        if(null == mDashboardFragment) {
        	
        	mDashboardFragment = DashboardFragment.newInstance();
        }
        
        fragmentTransaction.replace(R.id.main_container, mDashboardFragment);
        fragmentTransaction.commit();
	}
	
	/*
	 * The hardware menu button can show/hide the overflow menu 
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
	    
		super.onKeyUp(keyCode, event);
		
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			
	    	mApplication.doAction(ACTION_SHOW_OVERFLOW_ACTIONS);
	    }

	    return true;
	}

	public void doAction(Integer action, Object data) {
		
		switch(action) {
		
			case ACTION_SHOW_DASHBOARD:
				showDashboard();
				break;
			case ACTION_SHOW_EMAIL_SERVICES:
				startActivity(new Intent(this, EmailServicesActivity.class));
				break;
			case ACTION_SHOW_PARKING_LOTS:
				startActivity(new Intent(this, ParkingLotsActivity.class));
				break;
		}
	}
}
