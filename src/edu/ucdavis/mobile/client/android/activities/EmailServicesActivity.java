package edu.ucdavis.mobile.client.android.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import edu.ucdavis.mobile.client.android.ActionBusListener;
import edu.ucdavis.mobile.client.android.AppConstants;
import edu.ucdavis.mobile.client.android.MainApplication;
import edu.ucdavis.mobile.client.android.R;
import edu.ucdavis.mobile.client.android.fragments.EmailServiceDetailsFragment;
import edu.ucdavis.mobile.client.android.fragments.EmailServicesActionbarFragment;
import edu.ucdavis.mobile.client.android.fragments.EmailServicesFragment;
import edu.ucdavis.mobile.client.android.models.EmailService;

public class EmailServicesActivity extends FragmentActivity implements ActionBusListener, AppConstants {

	private MainApplication mMainApplication;
	private EmailServicesActionbarFragment mEmailServicesActionbarFragment;
	private EmailServicesFragment mEmailServicesFragment;
	private EmailServiceDetailsFragment mEmailServiceDetailsFragment;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	
        setContentView(R.layout.main);
	
        mMainApplication = (MainApplication) getApplication();
        
        mMainApplication.registerAction(this, ACTION_SHOW_EMAIL_SERVCIE_DETAIL);
        
        showEmailServices();
	}
	
	/*
	 * The hardware menu button can show/hide the overflow menu 
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
	    
		super.onKeyUp(keyCode, event);
		
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			
			mMainApplication.doAction(ACTION_SHOW_OVERFLOW_ACTIONS);
	    }

	    return true;
	}
	
	protected void showEmailServices() {
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		if(null == mEmailServicesActionbarFragment) {
			
			mEmailServicesActionbarFragment = EmailServicesActionbarFragment.newInstance();
			fragmentTransaction.replace(R.id.actionbar_container, mEmailServicesActionbarFragment);
		}
		
		if(null == mEmailServicesFragment) {
			
			mEmailServicesFragment = EmailServicesFragment.newInstance();
		}
		
		fragmentTransaction.replace(R.id.main_container, mEmailServicesFragment);
		fragmentTransaction.commit();
	}
	
	protected void showEmailServiceDetail(EmailService emailService) {
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		if(null == mEmailServicesActionbarFragment) {

			mEmailServicesActionbarFragment = EmailServicesActionbarFragment.newInstance();
			fragmentTransaction.replace(R.id.actionbar_container, mEmailServicesActionbarFragment);
		}

		if(null == mEmailServiceDetailsFragment) {
			
			mEmailServiceDetailsFragment = EmailServiceDetailsFragment.newInstance(emailService);
		}
		else {
			
			mEmailServiceDetailsFragment.setEmailService(emailService);
		}
		
		fragmentTransaction.replace(R.id.main_container, mEmailServiceDetailsFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}

	public void doAction(Integer action, Object data) {

		switch(action) {

		case ACTION_SHOW_EMAIL_SERVCIE_DETAIL:
			showEmailServiceDetail((EmailService) data);
			break;
		}
	}
}
