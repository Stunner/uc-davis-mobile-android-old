/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import edu.ucdavis.mobile.client.android.ActionBusListener;
import edu.ucdavis.mobile.client.android.AppConstants;
import edu.ucdavis.mobile.client.android.MainApplication;
import edu.ucdavis.mobile.client.android.R;
import edu.ucdavis.mobile.client.android.fragments.ParkingLotDetailsFragment;
import edu.ucdavis.mobile.client.android.fragments.ParkingLotsActionbarFragment;
import edu.ucdavis.mobile.client.android.fragments.ParkingLotsFragment;
import edu.ucdavis.mobile.client.android.models.ParkingLot;

public class ParkingLotsActivity extends FragmentActivity implements ActionBusListener, AppConstants {

	private MainApplication mMainApplication;
	private ParkingLotsActionbarFragment mParkingLotsActionbarFragment;
	private ParkingLotsFragment mParkingLotsFragment;
	private ParkingLotDetailsFragment mParkingLotDetailsFragment;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	
        setContentView(R.layout.main);
    	
        mMainApplication = (MainApplication) getApplication();
        
        mMainApplication.registerAction(this, ACTION_SHOW_PARKING_LOT_DETAIL);
        
        showParkingLots();
	}
	
	/*
	 * The hardware menu button can show/hide the overflow menu 
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
	    
		super.onKeyUp(keyCode, event);
		
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			
			mMainApplication.doAction(ACTION_SHOW_OVERFLOW_ACTIONS);
	    }

	    return true;
	}
	
	protected void showParkingLots() {
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		if(null == mParkingLotsActionbarFragment) {
			
			mParkingLotsActionbarFragment = ParkingLotsActionbarFragment.newInstance();
			fragmentTransaction.replace(R.id.actionbar_container, mParkingLotsActionbarFragment);
		}
		
		if(null == mParkingLotsFragment) {
			
			mParkingLotsFragment = ParkingLotsFragment.newInstance();
		}
		
		fragmentTransaction.replace(R.id.main_container, mParkingLotsFragment);
		fragmentTransaction.commit();
	}
	
	protected void showParkingLotDetail(ParkingLot parkingLot) {
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		if(null == mParkingLotsActionbarFragment) {
			
			mParkingLotsActionbarFragment = ParkingLotsActionbarFragment.newInstance();
			fragmentTransaction.replace(R.id.actionbar_container, mParkingLotsActionbarFragment);
		}
		
		if(null == mParkingLotDetailsFragment) {
			
			mParkingLotDetailsFragment = ParkingLotDetailsFragment.newInstance(parkingLot);
		}
		else {
			
			mParkingLotDetailsFragment.setParkingLotDetails(parkingLot.getName());
		}
		
		fragmentTransaction.replace(R.id.main_container, mParkingLotDetailsFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}
	
	public void doAction(Integer action, Object data) {
		
		switch(action) {
		
		case ACTION_SHOW_PARKING_LOT_DETAIL:
			showParkingLotDetail((ParkingLot) data);
			break;
		}
	}
}
