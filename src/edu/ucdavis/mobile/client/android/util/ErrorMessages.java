package edu.ucdavis.mobile.client.android.util;

import android.content.Context;
import android.widget.Toast;
import edu.ucdavis.mobile.client.android.AppConstants;
import edu.ucdavis.mobile.client.android.R;

public class ErrorMessages implements AppConstants {
	
	public static void display(int errorCode, Context context) {
		
		int messageRef = R.string.undefined_error;
		
		switch(errorCode) {
		
		case REST_ERROR:
			messageRef = R.string.action_bar_title_email;
			break;
		
		}
		
		Toast.makeText(context.getApplicationContext(), messageRef, Toast.LENGTH_SHORT).show();
	}
}
