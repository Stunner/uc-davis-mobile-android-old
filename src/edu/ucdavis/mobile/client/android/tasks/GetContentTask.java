/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.tasks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import edu.ucdavis.mobile.client.android.AppConstants;

public class GetContentTask extends AsyncTask<String, Void, String> {

	private GetContentTaskNotifier mNotifier;
	
	private HttpResponse mHttpResponse;
	private int mHttpStatusCodeOk;
	private int mHttpStatusCode;
	private int mErrorCode = AppConstants.NO_ERROR;
	
	/*
	 * @param notifier : Any class that implements the GetJsonTaskNotifier API
	 * @param httpStatusCodeOk : An HTTP status OK code e.g. HttpStatus.SC_OK (200)
	 */
	public GetContentTask(GetContentTaskNotifier notifier, int httpStatusCodeOk) {
		
		mNotifier = notifier;
		mHttpStatusCodeOk = httpStatusCodeOk;
	}
	
	@Override
	protected String doInBackground(String... urls) {
		
		if(urls.length == 0) {
			return null;
		}
		
		String url = urls[0];
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = null;
		
		try {
			
			httpGet = new HttpGet(url);
		}
		catch(IllegalArgumentException e) {
			
			return null;
		}
		
		// Checking if the task has been canceled 
		if(isCancelled()) {
			return null;
		}
		
		try {
			
			mHttpResponse = httpClient.execute(httpGet);
		}
		catch (ClientProtocolException e) {

			mErrorCode = AppConstants.REST_ERROR;
			return null;
		}
		catch (IOException e) {
			
			mErrorCode = AppConstants.REST_ERROR;
			return null;
		}
		
		if(null == mHttpResponse) {
			return null;
		}
		mHttpStatusCode = mHttpResponse.getStatusLine().getStatusCode();
		
		if(mHttpStatusCodeOk != mHttpStatusCode) {
			
			return null;
		}
		
		// Checking if the task has been canceled 
		if(isCancelled()) {
			return null;
		}
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		HttpEntity httpEntity = mHttpResponse.getEntity();
		
		if(null == httpEntity) {
			
			return null;
		}
		
		try {
		
			httpEntity.writeTo(out);
			out.close();
		}
		catch(IOException ioe) {
			// TODO
		}

		return out.toString();
	}
	
	@Override
	protected void onPostExecute(String content) {
		
		if(AppConstants.NO_ERROR != mErrorCode) {
		
			mNotifier.onError(mErrorCode);
		}
		
		mNotifier.doProcess(content);
	}
	
	@Override
	protected void onCancelled() {
		
		mNotifier.doCancel();
	}
}
