/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.tasks;

public interface GetContentTaskNotifier {

	public void doProcess(String content);
	
	public void doCancel();
	
	public void onError(int errorCode);
}
