/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android;

public interface RestConstants {

	// Parking
	public static final String PARKING_GET_LOT_NAMES = "http://taps.ucdavis.edu/parking_ws/LotInfo";
	public static final String PARKING_GET_LOT_DETAILS = "http://taps.ucdavis.edu/parking_ws/LotInfo/";
}
