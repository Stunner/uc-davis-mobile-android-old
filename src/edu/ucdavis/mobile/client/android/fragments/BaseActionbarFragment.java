/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import edu.ucdavis.mobile.client.android.ActionBusListener;
import edu.ucdavis.mobile.client.android.AppConstants;
import edu.ucdavis.mobile.client.android.MainApplication;
import edu.ucdavis.mobile.client.android.R;

/**
 * 
 * Base class for all actionbar fragments
 *
 */
abstract class BaseActionbarFragment extends Fragment implements ActionBusListener, AppConstants {
	
	private Activity mActivity;
	private ImageButton mImageButtonOverflow;
	private List<String> mOverflowActions;
	private ArrayAdapter<String> mArrayAdapter;
	
	protected MainApplication mMainApplication;
	protected ListView mOverflowActonList;
	protected ImageView mImageViewNavigatePrevious;
	protected ImageButton mImageButtonHome;
	protected TextView mTextViewTitle;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.actionbar_fragment, container, false);
	}
	
	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Keep a reference to the activity and application
		mActivity = getActivity();
		mMainApplication = (MainApplication) mActivity.getApplication();

		mImageViewNavigatePrevious = (ImageView) mActivity.findViewById(R.id.actionbar_iv_home);
		
		mTextViewTitle = (TextView) mActivity.findViewById(R.id.actionbar_tv_title);
		
		mImageButtonHome = (ImageButton) mActivity.findViewById(R.id.actionbar_ib_home);
		mImageButtonHome.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				mActivity.finish();
			}
		});
		
		mImageButtonOverflow = (ImageButton) mActivity.findViewById(R.id.actionbar_ib_overflow);
		mImageButtonOverflow.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
			
				toggleVisibility(mOverflowActonList);
			}
		});
		
		// Setup the overflow action list
		mOverflowActonList = (ListView) mActivity.findViewById(R.id.actionbar_lv_overflow);
		mOverflowActions = new ArrayList<String>();
		mArrayAdapter = getArrayAdapter();
		mOverflowActonList.setAdapter(mArrayAdapter);
		
		// Register action(s)
		mMainApplication.registerAction(this, ACTION_SHOW_OVERFLOW_ACTIONS);
	}
	
	/*
	 * Implementing ActionBusListener API
	 */
	public void doAction(Integer action, Object data) {

		switch(action) {
		
		case ACTION_SHOW_OVERFLOW_ACTIONS:
			toggleVisibility(mOverflowActonList);
			break;
		}
	}
	
	/*
	 * Derived classes need to implement this overflow action item
	 * click listener
	 */
	protected abstract OnItemClickListener getActionListener();
	
	protected void addOverflowActions(String[] actions) {

		mOverflowActions.clear();

		for(String action : actions) {

			mOverflowActions.add(action);
		}

		mArrayAdapter.notifyDataSetChanged();
	}
	
	/*
	 * Toggles a view's visibility
	 */
	protected void toggleVisibility(View view) {
		
		if(View.VISIBLE == view.getVisibility()) {
			
			view.setVisibility(View.GONE);
		}
		else {
			
			view.setVisibility(View.VISIBLE);
		}
	}
	
	/*
	 * Create ArrayAdapter for overflow action list. Also configuring view to use
	 * a white text color
	 */
	private ArrayAdapter<String> getArrayAdapter() {

		return new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, mOverflowActions) {

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View view = super.getView(position, convertView, parent);

				((TextView)view).setTextColor(Color.WHITE);

				return view;
			}
		};
	}
}
