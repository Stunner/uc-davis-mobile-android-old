/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import edu.ucdavis.mobile.client.android.AppConstants;
import edu.ucdavis.mobile.client.android.MainApplication;
import edu.ucdavis.mobile.client.android.RestConstants;
import edu.ucdavis.mobile.client.android.models.ParkingLot;
import edu.ucdavis.mobile.client.android.tasks.GetContentTask;
import edu.ucdavis.mobile.client.android.tasks.GetContentTaskNotifier;
import edu.ucdavis.mobile.client.android.util.ErrorMessages;

public class ParkingLotsFragment extends ListFragment implements GetContentTaskNotifier, AppConstants {

	private MainApplication mMainApplication;
	private Activity mActivity;
	
	private List<ParkingLot> mParkingLots;
	private ArrayAdapter<ParkingLot> mArrayAdapter;
	
	private GetContentTask mGetContentTask;
	
	public static ParkingLotsFragment newInstance() {
		
		ParkingLotsFragment parkingServiceFragment = new ParkingLotsFragment();
		
		return parkingServiceFragment;
	}
	
	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		
		mActivity = getActivity();
		mMainApplication = (MainApplication) mActivity.getApplication();
		
		mParkingLots = new ArrayList<ParkingLot>();
		mArrayAdapter = new ArrayAdapter<ParkingLot>(mActivity, android.R.layout.simple_list_item_1, mParkingLots);
		setListAdapter(mArrayAdapter);
		
		getParkingLots();
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		ParkingLot parkingLot = mParkingLots.get(position);
		mMainApplication.doAction(ACTION_SHOW_PARKING_LOT_DETAIL, parkingLot);
	}
	
	@Override
	public void onStop() {
		super.onStop();
		
		if(null != mGetContentTask) {
		
			mGetContentTask.cancel(true);
		}
	}
	
	private void getParkingLots() {
		
		String content = mMainApplication.getCacheValue(PARKING_LOT_CCK);
		
		if(null == content) {

			/*
			 * Get the content. Once the task got the content, it will call the doNotify(...) method
			 */
			mGetContentTask = new GetContentTask(this, HttpStatus.SC_OK);
			mGetContentTask.execute(RestConstants.PARKING_GET_LOT_NAMES);
		}
		else {
			
			updateList(content);
		}
	}

	/*
	 * GetJsonTaskNotifier API
	 */
	public void doProcess(String content) {
		
		if(null == content || content.length() == 0) {
			
			// TODO: Handle error : dialog
			return;
		}
		
		mMainApplication.putCacheValue(PARKING_LOT_CCK, content);
		
		updateList(content);
	}

	/*
	 * GetJsonTaskNotifier API
	 */
	public void doCancel() { /* Nothing todo */ }
	
	/*
	 * GetJsonTaskNotifier API
	 */
	public void onError(int errorCode) {
		
		ErrorMessages.display(errorCode, mActivity);
	}
	
	private void updateList(String content) {
		
		/*
		 * Remove double quotes from content
		 */
		String[] parkingLotNames = content.replaceAll("\"", "").split(",");
		
		for(String parkingLotName : parkingLotNames) {
			
			mParkingLots.add(new ParkingLot(parkingLotName));
		}
		
		mArrayAdapter.notifyDataSetChanged();
	}
}
