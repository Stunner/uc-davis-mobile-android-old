/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import edu.ucdavis.mobile.client.android.AppConstants;
import edu.ucdavis.mobile.client.android.MainApplication;
import edu.ucdavis.mobile.client.android.R;
import edu.ucdavis.mobile.client.android.activities.MainActivity;

public class DashboardFragment extends Fragment implements AppConstants {

	private MainApplication mMainApplication;
	private MainActivity mMainActivity;
	
	private Button mButtonMail;
	private Button mButtonMap;
	private Button mButtonNews;
	private Button mButtonResources;
	private Button mButtonTransit;
	private Button mButtonVideo;
	
	public static DashboardFragment newInstance() {

		DashboardFragment dashboardFragment = new DashboardFragment();

		return dashboardFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return inflater.inflate(R.layout.dashboard_fragment, container, false);
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mMainActivity = (MainActivity)getActivity();
		mMainApplication = (MainApplication) mMainActivity.getApplication();
		
		// Get reference to all the dashboard buttons
		mButtonMail = (Button) getActivity().findViewById(R.id.home_btn_mail);
		mButtonMap = (Button) getActivity().findViewById(R.id.home_btn_map);
		mButtonNews = (Button) getActivity().findViewById(R.id.home_btn_news);
		mButtonResources = (Button) getActivity().findViewById(R.id.home_btn_resources);
		mButtonTransit = (Button) getActivity().findViewById(R.id.home_btn_transit);
		mButtonVideo = (Button) getActivity().findViewById(R.id.home_btn_video);

		// Handle dashboard button click events
		mButtonMail.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				mMainApplication.doAction(ACTION_SHOW_EMAIL_SERVICES);
			}
		});

		mButtonMap.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				
				showShortMessage("DEBUG: Map");
			}
		});
		
		mButtonNews.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {

				showShortMessage("DEBUG: News");
			}
		});
		
		mButtonResources.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {

				showShortMessage("DEBUG: Resources");
			}
		});

		mButtonTransit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				showShortMessage("DEBUG: Transit");
			}
		});

		mButtonVideo.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				showShortMessage("DEBUG: Video");
			}
		});
		
	}

	private void showShortMessage(CharSequence message) {

		Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
	}
}
