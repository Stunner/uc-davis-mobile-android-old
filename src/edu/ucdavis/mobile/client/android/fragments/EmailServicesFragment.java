/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.fragments;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import edu.ucdavis.mobile.client.android.AppConstants;
import edu.ucdavis.mobile.client.android.MainApplication;
import edu.ucdavis.mobile.client.android.R;
import edu.ucdavis.mobile.client.android.models.EmailService;

public class EmailServicesFragment extends ListFragment implements AppConstants {

	private MainApplication mMainApplication;
	private Activity mActivity;
	
	private List<EmailService> mEmailServices;
	private ArrayAdapter<EmailService> mArrayAdapter;
	
	public static EmailServicesFragment newInstance() {
		
		EmailServicesFragment emailServicesFragment = new EmailServicesFragment();
		
		return emailServicesFragment;
	}
	
	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	
		mActivity = getActivity();
		mMainApplication = (MainApplication) mActivity.getApplication();
		
		// Get Email Services Objects
		mEmailServices = getEmailServices();
		mArrayAdapter = new ArrayAdapter<EmailService>(mActivity, android.R.layout.simple_list_item_1, mEmailServices);
		
		setListAdapter(mArrayAdapter);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
	
		EmailService emailService = mEmailServices.get(position);
		mMainApplication.doAction(ACTION_SHOW_EMAIL_SERVCIE_DETAIL, emailService);
	}
	
	private List<EmailService> getEmailServices() {
		
		List<EmailService> emailServcies = new ArrayList<EmailService>();
		
		InputStream inputStream = mActivity.getResources().openRawResource(R.raw.email_services);
		
		byte [] buffer = null;
		
		try {
		
			buffer = new byte[inputStream.available()];
			
			while (inputStream.read(buffer) != -1);
		}
		catch(IOException ioe) {
			
			// TODO
			Log.e(UCDM_LOG_TAG, "IOException: getMailService");
		}
		catch(Exception e) {
			
			// TODO
			Log.e(UCDM_LOG_TAG, "Exception: getMailService");
		}
		
		String jsonData = new String(buffer);
		
		JSONObject emailServicesObj = null;
		
		try {
			
			emailServicesObj = new JSONObject(jsonData);
			JSONArray entries = emailServicesObj.getJSONArray("email_services");
			
			for(int i = 0; i < entries.length(); i++) {
			
				EmailService emailService = new EmailService(entries.getJSONObject(i));
				emailServcies.add(emailService);
			}
		}
		catch(JSONException e) {
			
			// TODO
			Log.e(UCDM_LOG_TAG, "JSONException: getMailService");
		}
		
		return emailServcies;
	}
}
