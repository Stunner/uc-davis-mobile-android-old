/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import edu.ucdavis.mobile.client.android.MainApplication;
import edu.ucdavis.mobile.client.android.R;
import edu.ucdavis.mobile.client.android.dialogs.HelpDialog;

public class DashboardActionbarFragment extends BaseActionbarFragment  {
	
	private Activity mActivity;
	private MainApplication mMainApplication;
	
	public static DashboardActionbarFragment newInstance() {
		
		DashboardActionbarFragment actionbarFragment = new DashboardActionbarFragment();
		return actionbarFragment;
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mActivity = getActivity();
		mMainApplication = (MainApplication) mActivity.getApplication();

		mImageViewNavigatePrevious.setVisibility(View.INVISIBLE);
		
		// Making sure that the home button doesn't do anything when viewing the dashboard
		mImageButtonHome.setOnClickListener(null);
		
		mOverflowActonList.setOnItemClickListener(getActionListener());
		addOverflowActions(getResources().getStringArray(R.array.dashboard_actions));

		// Register action here if needed
	}
	
	
	@Override
	protected OnItemClickListener getActionListener() {

		return new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				mOverflowActonList.setVisibility(View.GONE);

				switch(position) {

				case LIST_ACTION_HELP:
					showHelp();
					break;
				case LIST_ACTION_INFO:
					showInfo();
					break;
				case LIST_ACTION_PARKING:
					mMainApplication.doAction(ACTION_SHOW_PARKING_LOTS);
					break;
				}
			}
		};
	}
	
	private void showHelp() {

		HelpDialog helpDialog = new HelpDialog(mActivity);
		helpDialog.setHelp(getResources().getString(R.string.help_content_dashboard));
		helpDialog.show();
	}
	
	private void showInfo() {
		
		// TODO:
	}

	@Override
	public void doAction(Integer action, Object data) {
		super.doAction(action, data);
	
		// TODO: Add Dashboard action handling
		// switch
	}
}
