/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import edu.ucdavis.mobile.client.android.R;
import edu.ucdavis.mobile.client.android.dialogs.HelpDialog;

public class ParkingLotsActionbarFragment extends BaseActionbarFragment {
	
	private Activity mActivity;
	
	public static ParkingLotsActionbarFragment newInstance() {
		
		ParkingLotsActionbarFragment actionbarFragment = new ParkingLotsActionbarFragment();
		return actionbarFragment;
	}
	
	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mActivity = getActivity();
		
		mTextViewTitle.setText(getString(R.string.action_bar_title_parking));
		mOverflowActonList.setOnItemClickListener(getActionListener());
		addOverflowActions(getResources().getStringArray(R.array.parking_lots_actions));
		
		// Register action here if needed
	}
	
	@Override
	protected OnItemClickListener getActionListener() {

		return new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				mOverflowActonList.setVisibility(View.GONE);

				switch(position) {

				case LIST_ACTION_HELP:
					showHelp();
					break;
				}
			}
		};
	}
	
	private void showHelp() {
		
		HelpDialog helpDialog = new HelpDialog(mActivity);
		helpDialog.setHelp(getResources().getString(R.string.help_content_parking_lots));
		helpDialog.show();
	}

	@Override
	public void doAction(Integer action, Object data) {
		super.doAction(action, data);
		
		// TODO: Add Email Services action handling
		// switch
	}
}
