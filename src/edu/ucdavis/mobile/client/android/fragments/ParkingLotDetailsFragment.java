/****************************************************************
 *
 * Copyright (c) 2012 The University of California Davis
 * 
 * Author: Thomas Amsler, tamsler@gmail.com
 *
 ****************************************************************/

package edu.ucdavis.mobile.client.android.fragments;

import org.apache.http.HttpStatus;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import edu.ucdavis.mobile.client.android.R;
import edu.ucdavis.mobile.client.android.RestConstants;
import edu.ucdavis.mobile.client.android.models.ParkingLot;
import edu.ucdavis.mobile.client.android.models.ParkingLotDetail;
import edu.ucdavis.mobile.client.android.tasks.GetContentTask;
import edu.ucdavis.mobile.client.android.tasks.GetContentTaskNotifier;
import edu.ucdavis.mobile.client.android.util.ErrorMessages;

public class ParkingLotDetailsFragment extends Fragment implements GetContentTaskNotifier {
	
	private Activity mActivity;
	private ParkingLotDetail mParkingLotDetail;
	private TextView mTextView;
	private GetContentTask mGetContentTask;
	
	public static ParkingLotDetailsFragment newInstance(ParkingLot parkingLot) {
		
		ParkingLotDetailsFragment parkingLotDetailsFragment = new ParkingLotDetailsFragment();
		parkingLotDetailsFragment.setParkingLotDetails(parkingLot.getName());
		
		return parkingLotDetailsFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return inflater.inflate(R.layout.parking_lot_details_fragment, container, false);
	}
	
	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	
		mActivity = getActivity();
		mTextView = (TextView) mActivity.findViewById(R.id.textView1);
		
		mGetContentTask = new GetContentTask(this, HttpStatus.SC_OK);
		mGetContentTask.execute(RestConstants.PARKING_GET_LOT_DETAILS + mParkingLotDetail.getName());
	}

	public void setParkingLotDetails(String parkingLotName) {
		
		mParkingLotDetail = new ParkingLotDetail(parkingLotName);
	}
	

	@Override
	public void onStop() {
		super.onStop();
		
		if(null != mGetContentTask) {
		
			mGetContentTask.cancel(true);
		}
	}
	
	/*
	 * GetJsonTaskNotifier API
	 */
	public void doProcess(String content) {
		
		mTextView.setText(content);
	}

	/*
	 * GetJsonTaskNotifier API
	 */
	public void doCancel() { /* Nothing todo */ }
	
	/*
	 * GetJsonTaskNotifier API
	 */
	public void onError(int errorCode) {
		
		ErrorMessages.display(errorCode, mActivity);
	}
}
