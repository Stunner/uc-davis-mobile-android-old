package edu.ucdavis.mobile.client.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import edu.ucdavis.mobile.client.android.AppConstants;
import edu.ucdavis.mobile.client.android.R;
import edu.ucdavis.mobile.client.android.models.EmailService;

public class EmailServiceDetailsFragment extends Fragment implements AppConstants {

	private Activity mActivity;
	private WebView mWebView;
	private EmailService mEmailService;
	
	public static EmailServiceDetailsFragment newInstance(EmailService emailService) {
		
		EmailServiceDetailsFragment emailServiceDetailsFragment = new EmailServiceDetailsFragment();
		emailServiceDetailsFragment.setEmailService(emailService);
		return emailServiceDetailsFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return inflater.inflate(R.layout.email_service_details_fragment, container, false);
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mActivity = getActivity();
		mWebView = (WebView) mActivity.findViewById(R.id.emal_service_webview);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.getSettings().setJavaScriptEnabled(true);
				view.loadUrl(url);
				return false;
			}
		});

		mWebView.requestFocus(View.FOCUS_DOWN);
		mWebView.setOnTouchListener(new View.OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
					case MotionEvent.ACTION_UP:
						if (!v.hasFocus()) {
							v.requestFocus();
						}
					break;
				}
				return false;
			}
		});

		Log.i("DEGUG", "loading url ... " + mEmailService.getUrl());
		mWebView.loadUrl(mEmailService.getUrl());
	}
	
	public void setEmailService(EmailService emailService) {
		
		this.mEmailService = emailService;
	}
}
