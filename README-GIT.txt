GIT NOTES:

# Clone repository:
git clone git@bitbucket.org:Stunner/uc-davis-mobile-android.git
cd uc-davis-mobile-android

# Track develop branch
git checkout --track -b develop origin/develop

# Getting changes
cd uc-davis-mobile-android
git pull

# Committing changes
git add <files>
git commit -m "Some comment. Reference bitbucket's issue number if appropriate"
git push

# Merging develop and master
git checkout master
git merge --no-ff develop
git push

# Switch from master to develop branch
git checkout develop




